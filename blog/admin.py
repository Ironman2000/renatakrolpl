from django.contrib import admin
from .models import BlogPost, BlogPhoto

admin.site.register(BlogPost)
admin.site.register(BlogPhoto)
