from django.db import models
from django.utils import timezone


class BlogPost(models.Model):
    title = models.CharField(max_length=128, blank=False)
    description = models.TextField(default="", blank=False)
    photo = models.ImageField(upload_to='blog_photos', blank=True, null=True)
    text = models.TextField(default="", blank=False)
    created_at = models.DateField(editable=False, default=timezone.now)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if not self.id:
            self.created_at = timezone.now()

        return super(BlogPost, self).save(*args, **kwargs)


class BlogPhoto(models.Model):
    post = models.ForeignKey(BlogPost, on_delete=models.CASCADE, blank=True, null=True)
    photo = models.ImageField(upload_to='blog_photos', blank=True, null=True)
    isHeader = models.BooleanField()

    def __str__(self):
        if self.isHeader:
            return "Header"
        else:
            return "Blog photo " + str(self.id)

    def save(self, *args, **kwargs):
        if self.isHeader:
            try:
                tmp = BlogPhoto.objects.get(isHeader=True)
                tmp.isHeader = False
                tmp.save()
            except BlogPhoto.DoesNotExist:
                pass

        return super(BlogPhoto, self).save(*args, **kwargs)
