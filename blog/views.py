from django.shortcuts import render
from .models import BlogPost, BlogPhoto


def home_page(request):
    posts = BlogPost.objects.all()
    header = BlogPhoto.objects.get(isHeader=True)

    return render(request, 'index.html', {'posts': posts, 'header': header})


def blog_page(request, id):
    post = BlogPost.objects.get(pk=id)
    images = BlogPhoto.objects.filter(post=post)

    n = len(images)

    if n <= 3:
        return render(request, 'blog_post.html', {'post': post, 'photos': images})
    else:
        photos = []

        for i in images:
            photos.append(i)

        photo = []

        photo.append(photos[0])
        photo.append(photos[1])
        photo.append(photos[2])

        x = 3
        y = len(photos)

        while x < y:
            photos[x - 3] = photos[x]
            x += 1

        return render(request, 'blog_post2.html', {'post': post, 'photo': photo, 'photos': photos})
