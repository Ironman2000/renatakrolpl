from django.urls import path
from .views import blog_page

app_name = 'blog'

urlpatterns = [
    path('<int:id>/', blog_page, name="blog_page"),
]
