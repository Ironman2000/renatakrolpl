from django.db import models
from django.utils import timezone


class VoicePost(models.Model):
    title = models.CharField(max_length=128, blank=False)
    description = models.TextField(default="", blank=False)
    photo = models.ImageField(upload_to='voice_photos', blank=True, null=True)
    text = models.TextField(default="", blank=False)
    created_at = models.DateField(editable=False, default=timezone.now)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if not self.id:
            self.created_at = timezone.now()

        return super(VoicePost, self).save(*args, **kwargs)


class VoicePhoto(models.Model):
    post = models.ForeignKey(VoicePost, on_delete=models.CASCADE, blank=True, null=True)
    photo = models.ImageField(upload_to='voice_photos', blank=True, null=True)
    isHeader = models.BooleanField()

    def __str__(self):
        if self.isHeader:
            return "Header"
        else:
            return "Voice photo " + str(self.id)

    def save(self, *args, **kwargs):
        if self.isHeader:
            try:
                tmp = VoicePhoto.objects.get(isHeader=True)
                tmp.isHeader = False
                tmp.save()
            except VoicePhoto.DoesNotExist:
                pass

        return super(VoicePhoto, self).save(*args, **kwargs)
