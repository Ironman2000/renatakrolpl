from django.shortcuts import render
from .models import VoicePost, VoicePhoto


def voice_page(request):
    posts = VoicePost.objects.all()
    header = VoicePhoto.objects.get(isHeader=True)

    return render(request, 'voice_page.html', {'posts': posts, 'header': header})


def voice_post(request, id):
    post = VoicePost.objects.get(pk=id)
    images = VoicePhoto.objects.filter(post=post)

    n = len(images)

    if n <= 3:
        return render(request, 'voice_post.html', {'post': post, 'photos': images})
    else:
        photos = []

        for i in images:
            photos.append(i)

        photo = []

        photo.append(photos[0])
        photo.append(photos[1])
        photo.append(photos[2])

        x = 3
        y = len(photos)

        while x < y:
            photos[x - 3] = photos[x]
            x += 1

        return render(request, 'voice_post2.html', {'post': post, 'photo': photo, 'photos': photos})
