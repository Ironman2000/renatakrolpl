from django.contrib import admin
from .models import VoicePost, VoicePhoto


admin.site.register(VoicePost)
admin.site.register(VoicePhoto)
