from django.urls import path
from .views import voice_page, voice_post

app_name = 'voice'

urlpatterns = [
    path('', voice_page, name="voice_page"),
    path('<int:id>/', voice_post, name="voice_post"),
]
