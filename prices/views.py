from django.shortcuts import render
from .models import Price


def prices_page(request):
    prices = Price.objects.all()

    return render(request, 'prices.html', {'prices': prices})
