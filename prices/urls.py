from django.urls import path
from .views import prices_page

app_name = 'prices'

urlpatterns = [
    path('', prices_page, name="prices_page"),
]
