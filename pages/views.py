from django.shortcuts import render
from blog.models import BlogPhoto, BlogPost
from yoga.models import YogaPhoto, YogaPost
from voice.models import VoicePhoto, VoicePost
from pilates.models import PilatesPhoto, PilatesPost


def about_me(request):
    blog = BlogPhoto.objects.all()
    yoga = YogaPhoto.objects.all()
    voice = VoicePhoto.objects.all()
    pilates = PilatesPhoto.objects.all()

    cblog = BlogPost.objects.all()
    cyoga = YogaPost.objects.all()
    cvoice = VoicePost.objects.all()
    cpilates = PilatesPost.objects.all()

    context = {
        'blog': blog,
        'yoga': yoga,
        'voice': voice,
        'pilates': pilates,
        'cblog': cblog,
        'cyoga': cyoga,
        'cvoice': cvoice,
        'cpilates': cpilates,
    }

    return render(request, 'about_me.html', context=context)


def contact(request):
    return render(request, 'contact.html')


def gallery(request):
    blog = BlogPhoto.objects.all()
    yoga = YogaPhoto.objects.all()
    voice = VoicePhoto.objects.all()
    pilates = PilatesPhoto.objects.all()

    cblog = BlogPost.objects.all()
    cyoga = YogaPost.objects.all()
    cvoice = VoicePost.objects.all()
    cpilates = PilatesPost.objects.all()

    photos = []

    for b in blog:
        photos.append(b)

    for y in yoga:
        photos.append(y)

    for v in voice:
        photos.append(v)

    for p in pilates:
        photos.append(p)

    for blog in cblog:
        photos.append(blog)

    for yoga in cyoga:
        photos.append(yoga)

    for voice in cvoice:
        photos.append(voice)

    for pilates in cpilates:
        photos.append(pilates)

    firstPhoto = photos[0]
    n = len(photos)
    x = 1

    while x < n:
        photos[x - 1] = photos[x]
        x += 1

    if n <= 4:
        return render(request, 'gallery1.html', {'n': n, 'firstPhoto': firstPhoto, 'photos': photos})
    else:
        photo = []
        a = 0

        while a < 3:
            photo.append(photos[a])
            a += 1

        a = 3
        y = n - 1

        while a < y:
            photos[a - 3] = photos[a]
            a += 1

        return render(request, 'gallery2.html', {'n': n, 'firstPhoto': firstPhoto, 'photo': photo, 'photos': photos})
