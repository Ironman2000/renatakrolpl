from django.urls import path
from blog.views import home_page
from .views import about_me, contact, gallery

app_name = 'pages'

urlpatterns = [
    path('', home_page, name="home_page"),
    path('o-mnie/', about_me, name="about_me"),
    path('kontakt/', contact, name="contact"),
    path('galeria/', gallery, name="gallery"),
]
