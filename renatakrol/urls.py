from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('pages.urls')),
    path('blog/', include('blog.urls')),
    path('yoga/', include('yoga.urls')),
    path('voice/', include('voice.urls')),
    path('pilates/', include('pilates.urls')),
    path('cennik/', include('prices.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
