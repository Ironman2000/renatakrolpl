from django.shortcuts import render
from .models import YogaPost, YogaPhoto


def yoga_page(request):
    posts = YogaPost.objects.all()
    header = YogaPhoto.objects.get(isHeader=True)

    return render(request, 'yoga_page.html', {'posts': posts, 'header': header})


def yoga_post(request, id):
    post = YogaPost.objects.get(pk=id)
    images = YogaPhoto.objects.filter(post=post)

    n = len(images)

    if n <= 3:
        return render(request, 'yoga_post.html', {'post': post, 'photos': images})
    else:
        photos = []

        for i in images:
            photos.append(i)

        photo = []

        photo.append(photos[0])
        photo.append(photos[1])
        photo.append(photos[2])

        x = 3
        y = len(photos)

        while x < y:
            photos[x - 3] = photos[x]
            x += 1

        return render(request, 'yoga_post2.html', {'post': post, 'photo': photo, 'photos': photos})
