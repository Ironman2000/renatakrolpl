from django.db import models
from django.utils import timezone


class YogaPost(models.Model):
    title = models.CharField(max_length=128, blank=False)
    description = models.TextField(default="", blank=False)
    photo = models.ImageField(upload_to='yoga_photos', blank=True, null=True)
    text = models.TextField(default="", blank=False)
    created_at = models.DateField(editable=False, default=timezone.now)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if not self.id:
            self.created_at = timezone.now()

        return super(YogaPost, self).save(*args, **kwargs)


class YogaPhoto(models.Model):
    post = models.ForeignKey(YogaPost, on_delete=models.CASCADE, blank=True, null=True)
    photo = models.ImageField(upload_to='yoga_photos', blank=True, null=True)
    isHeader = models.BooleanField()

    def __str__(self):
        if self.isHeader:
            return "YogaHeader"
        else:
            return "Yoga photo " + str(self.id)

    def save(self, *args, **kwargs):
        if self.isHeader:
            try:
                tmp = YogaPhoto.objects.get(isHeader=True)
                tmp.isHeader = False
                tmp.save()
            except YogaPhoto.DoesNotExist:
                pass

        return super(YogaPhoto, self).save(*args, **kwargs)
