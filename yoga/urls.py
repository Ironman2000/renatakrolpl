from django.urls import path
from .views import yoga_page, yoga_post

app_name = 'yoga'

urlpatterns = [
    path('', yoga_page, name="yoga_page"),
    path('<int:id>/', yoga_post, name="yoga_post"),
]
