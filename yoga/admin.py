from django.contrib import admin
from .models import YogaPost, YogaPhoto

admin.site.register(YogaPost)
admin.site.register(YogaPhoto)
