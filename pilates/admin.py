from django.contrib import admin
from .models import PilatesPost, PilatesPhoto


admin.site.register(PilatesPost)
admin.site.register(PilatesPhoto)
