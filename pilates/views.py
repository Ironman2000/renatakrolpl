from django.shortcuts import render
from .models import PilatesPost, PilatesPhoto


def pilates_page(request):
    posts = PilatesPost.objects.all()
    header = PilatesPhoto.objects.get(isHeader=True)

    return render(request, 'pilates_page.html', {'posts': posts, 'header': header})


def pilates_post(request, id):
    post = PilatesPost.objects.get(pk=id)
    images = PilatesPhoto.objects.filter(post=post)

    n = len(images)

    if n <= 3:
        return render(request, 'pilates_post.html', {'post': post, 'photos': images})
    else:
        photos = []

        for i in images:
            photos.append(i)

        photo = []

        photo.append(photos[0])
        photo.append(photos[1])
        photo.append(photos[2])

        x = 3
        y = len(photos)

        while x < y:
            photos[x - 3] = photos[x]
            x += 1

        return render(request, 'pilates_post2.html', {'post': post, 'photo': photo, 'photos': photos})
