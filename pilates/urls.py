from django.urls import path
from .views import pilates_page, pilates_post

app_name = 'pilates'

urlpatterns = [
    path('', pilates_page, name="pilates_page"),
    path('<int:id>/', pilates_post, name="pilates_post"),
]
