from django.apps import AppConfig


class PilatesConfig(AppConfig):
    name = 'pilates'
